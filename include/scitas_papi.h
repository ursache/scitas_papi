#ifndef SCITAS_PAPI_H
#define SCITAS_PAPI_H

//
// A simple PAPI counters wrappers.
// authors: G. Fourestey (gilles.fourestey@epfl.ch)
//
// usage: export papi/native counters in PAPI_EVENTS separated by "|"
// ex: export PAPI_EVENTS="PAPI_FPU_TOT|DISPATCHED_FPU_OPS"
//
// see http://oprofile.sourceforge.net/docs/ for a list of native events on all architectures
// run papi_events utility to get the list of papi_events availalble on an architecture
//


#ifdef __cplusplus
extern "C"
{
#endif

#include <papi.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

        typedef char* string;

        int EventSet = PAPI_NULL;
        static int Events[128];
        static string sEvents[128];

        static int NUMEVENTS = 0;
        static int NOEVENT   = 0;
        static long_long values[128*2];

        static int               setup = 0;
        static int               flops = 0;
        static float     real_time, proc_time, mflops;
        static long long flpins;
        static int       papi_error;

        static int       debugFlag;
        static int       once;

        static int       is_counting; // keep track of whether counters have been started


#define PAPI_START papi_start_counters();
#define PAPI_ACCUM papi_accum_counters();
#define PAPI_STOP  papi_stop_counters();
#define PAPI_FLUSH papi_flush_counters();
#define PAPI_PRINT papi_print_results();


        void papi_print_error(int ierr)
        {
                char errstring[PAPI_MAX_STR_LEN];
                PAPI_perror(ierr, errstring, PAPI_MAX_STR_LEN );

                printf("PAPI error %s\n", errstring );

        }

        void papi_setup()
        {

                /* Initialize the library */
                papi_error = PAPI_library_init(PAPI_VER_CURRENT);
                if (papi_error != PAPI_VER_CURRENT) 
                {
                        printf("PAPI library init error!\n");
                        exit(1);
                }

                int num_hwcntrs, i;

                papi_error = num_hwcntrs = PAPI_num_counters();
                if (papi_error <= PAPI_OK)
                        papi_print_error(papi_error);

                string papi_debug = getenv("PAPI_DEBUG");
                int debugFlag = (papi_debug != NULL);

                if (debugFlag)
                {
                        printf("This system has %d available counters.\n", num_hwcntrs);
                }

                string papi_counters = getenv("PAPI_EVENTS");
                if (papi_counters == NULL) papi_counters="";
                if (debugFlag)
                        printf("PAPI_EVENTS = %s\n", papi_counters);

                string result = NULL;
                char  delim[] = "|";


                papi_error = PAPI_create_eventset(&EventSet);
                if (papi_error != PAPI_OK)
                        printf("Could not create the EventSet: %d\n", papi_error);
                result = strtok( papi_counters, delim );
                while( result != NULL )
                {
                        // first verify that the event string is valid, before adding to sEvents and Events
                        int event_code;

                        if (strcmp(result, "PAPI_flops") == 0)
                        {
                                NUMEVENTS = 1;
                                flops     = 1;
                                setup     = 1;
                                return;
                        }

                        papi_error = PAPI_event_name_to_code(result, &event_code);
                        if (papi_error!=PAPI_OK)
                        {
                                printf("Event %s not recognised\n", result);
                        }
                        else
                        {
                                papi_error = PAPI_add_event(EventSet, event_code);
                                if (papi_error != PAPI_OK)  
                                {
                                        printf("Could not add event %s to the event set: %d, %d\n", result, papi_error, PAPI_OK);
                                }
                                else // event was successfully added, so update our lists
                                {
                                        sEvents[NUMEVENTS] = result;
                                        Events[NUMEVENTS] = event_code;
                                        NUMEVENTS++;
                                }
                        }

                        result = strtok( NULL, delim );
                }

                if (debugFlag && NUMEVENTS)
                {
                        printf("We will count %d events.\n", NUMEVENTS);
                }
                else if(debugFlag && !NUMEVENTS)
                {
                        if (debugFlag)
                                printf("No event selected\n");

                        setup = 1;
                        return;
                }


                if (NUMEVENTS > 127)
                {
                        printf("Too many events selected\n");
                        exit(-1);
                }


                if (debugFlag && NUMEVENTS)
                        for (i = 0; i < NUMEVENTS; i++)
                                printf("Event %d out of %d = %s\n", i, NUMEVENTS, sEvents[i]);

                setup = 1;
                is_counting = 0;
                once  = 0;
        }


        void papi_start_counters()
        {
                if (!setup) papi_setup();

                if (NUMEVENTS == 0) return;

                if (flops)
                {
                        papi_error=PAPI_flops( &real_time, &proc_time, &flpins, &mflops);
                        if (papi_error != PAPI_OK) papi_print_error(papi_error);
                        return;
                }

                if(debugFlag){
                        if( is_counting ){
                                fprintf(stderr, "SCITAS_PAPI error : attempt to start counters when they are already running\n" );
                                exit(-1);
                        }
                }
                // replace lower-level PAPI_start() with higher-level PAPI_start_counters()
                //papi_error = PAPI_start(EventSet);
                papi_error = PAPI_start_counters(Events, NUMEVENTS);
                if (papi_error != PAPI_OK) papi_print_error(papi_error);
                is_counting = 1;
        }


        void papi_stop_counters()
        {
                if (NUMEVENTS == 0) return;

                if (flops)
                {
                        papi_error = PAPI_flops( &real_time, &proc_time, &flpins, &mflops);
                        if (papi_error != PAPI_OK) papi_print_error(papi_error);
                        return;
                }

                if(debugFlag){
                        if( !is_counting ){
                                fprintf(stderr, "SCITAS_PAPI error : attempt to stop counters that have not been started\n" );
                                exit(-1);
                        }
                }
                // replace lower-level PAPI_stop() with higher-level PAPI_stop_counters()
                papi_error = PAPI_stop_counters(values, NUMEVENTS);
                if (papi_error != PAPI_OK) papi_print_error(papi_error);
                is_counting = 0;
        }

        // stop the counters, and add them values
        void papi_accum_counters()
        {
                if (NUMEVENTS == 0) return;

                if (flops)
                {
                        papi_error = PAPI_flops( &real_time, &proc_time, &flpins, &mflops);
                        if (papi_error != PAPI_OK) papi_print_error(papi_error);
                        return;
                }

                if(debugFlag){
                        if( !is_counting ){
                                fprintf(stderr, "SCITAS_PAPI error : attempt to stop counters that have not been started\n" );
                                exit(-1);
                        }
                }
                papi_error = PAPI_accum_counters(values, NUMEVENTS);
                if (papi_error != PAPI_OK) papi_print_error(papi_error);
                // now stop the counters, and write after the end of values to avoid overwriting counters.
                papi_error = PAPI_stop_counters(values+128, NUMEVENTS);
                if (papi_error != PAPI_OK) papi_print_error(papi_error);
                is_counting = 0;
        }


        void papi_flush_counters()
        {
                if (NUMEVENTS == 0) return;
                once = 0;

                if(debugFlag){
                        if( !is_counting ){
                                fprintf(stderr, "SCITAS_PAPI error : attempt to stop counters that have not been started\n" );
                                exit(-1);
                        }
                }
                int ii;

                memset(&values[0], 0, 128*2*sizeof(double));
        }


        void papi_print_results()
        {
                if (once)
                {
                        return;
                }
                once = 1;
                printf("\n");
                if (NUMEVENTS == 0) return;

                if (flops)
                {
                        printf("Real_time:\t%f\nProc_time:\t%f\nTotal flpins:\t%lld\nMFLOPS:\t\t%f\n",
                                        real_time, proc_time, flpins, mflops);
                        return;
                }


                char eventName[PAPI_MAX_STR_LEN];
                int i;

                for(i = 0; i < NUMEVENTS; i++)
                {
                        papi_error = PAPI_event_code_to_name(Events[i], eventName);
                        if (papi_error != PAPI_OK) papi_print_error(papi_error);
                        printf("Event %s:\t\t %lu\n", eventName, values[i]);
                }
        }

#ifdef __cplusplus
}
#endif

#endif // SCITAS_PAPI_H
